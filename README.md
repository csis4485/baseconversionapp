# BaseConverter

#------WINDOWS Gradle and Java jdk Installation-----
reference
```http://www.vogella.com/tutorials/Gradle/article.html#installing-gradle-on-windows```
#DOWNLOAD and Install Gradle & Java 8:
Java
```
http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
```

# RUN THE INSTALLER 
	note:if you get an error about install location...ignore it

Gradle  
```
https://gradle.org/gradle-download/?_ga=1.176148897.2056390280.1476385949
```
 	Unzip to C:\ProgramFiles\gradle-3.1\bin

#Update the PATH Environment Variable

1. Click Start, then Control Panel, then System.

2. Click Advanced, then Environment Variables.

3. Add the location of the bin folder of the JDK installation to the PATH variable in System Variables. 

The following is a typical value for the PATH variable:

C:\WINDOWS\system32;C:\WINDOWS;C:\Program Files\Java\jdk1.8.0\bin; C:\ProgramFiles\gradle-3.1\bin

	Note:
	The PATH environment variable is a series of directories separated by semicolons (;) and is not case-sensitive. Microsoft Windows looks for programs in the PATH directories in order, from left to right.

You should only have one bin directory for a JDK in the path at a time. Those following the first instance are ignored.

The new path takes effect in each new command window you open after setting the PATH variable.

#Clone project 
```
$ git clone https://eichen82110@bitbucket.org/csis4485/baseconversionapp.git
```

#Import into Injellij
try{'import as gradle project'}
**if this doesnt work use 'open project'
then it will ask to import with gradle....do it and user custom gradle wrapper

#---------MAC Install--------
if you dont have homebrew....get it
```
$ brew install gradle
```

## Contributing
If you'd like to contribute to the project and are new to the git workflow, follow the steps below

1. Fork the repository

2. Clone your forked repo and navigate to the newly created directory

    ```
    git clone [url to repo]
    cd baseconverter
    ```
    
3. Checkout a new branch 

    ```
    git checkout -b branch_name
    ```
    
4. Make the changes to the directory that you'd like to contribute

5. Add the changes you've made to git and create a new commit

    ```
    git add .
    git commit
    ```
    
    Type "i",enter your commit message and type Escape, ":wq", then Enter to write the file and exit vi
    
6. Push your changes to your forked repo

    ```
    git push origin [branch_name]
    ```    
     
 8. If you are seeing the message: `Your branch is ahead of 'origin/master' by X commits.` and you want it to go away,
 simply push to your local fork:
 
     ```
     git push origin
     ```
 
 9. If you want to clean up old branches, you can use this command:
 
     ```
     git remote prune origin
     ```
     ## Setup
     In order to use this application as a fake client to authenticate you need to perform the following steps:
     
     1. Add the following line to `/etc/hosts`:
     
         ```
         127.0.0.1   localhost.com
         ```
     
     2. Run gradlew to download and build the project
     
         ```
         ./gradlew
         ```
     
     3. Do a bootRun of gradle to run the application, it will be started in your browser at localhost:8080 
     
         ```
         ./gradlew bootRun
         ```
     
         Note: `Building 92% > :adapter:bootRun` will be printed to the console when the app is ready.
     
     4. Open `http://localhost:8080`