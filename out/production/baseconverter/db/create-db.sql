CREATE TABLE IF NOT EXISTS users (
ID INTEGER IDENTITY PRIMARY KEY NOT NULL,
username varchar(45) NOT NULL,
password varchar(255) NOT NULL,
email varchar(255) NOT NULL,
authorities varchar(45) NOT NULL,
enabled boolean DEFAULT 1,
);