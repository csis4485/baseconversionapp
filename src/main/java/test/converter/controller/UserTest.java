package test.converter.controller;

import converter.model.User;
import converter.service.GradeService;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UserTest {

    private User user = new User();
    private GradeService gradeService = new GradeService();

    /**
     * Tests setting users grades and retrieving them using GradeService
     * Method: setUserEasyGrade(String conversionType, User user, String grade)
     *
     */
    @Test
    public void testSetUserGrades() throws Exception {
        gradeService.setUserEasyGrade("decimalToBinary", user, "100");
        gradeService.setUserHardGrade("decimalToBinary", user, "100");
        gradeService.setUserMediumGrade("decimalToBinary", user, "100");
        assertEquals(String.valueOf(user.getDecimalBinaryEasyGrade()), "100");
        assertEquals(String.valueOf(user.getDecimalBinaryMediumGrade()), "100");
        assertEquals(String.valueOf(user.getDecimalBinaryHardGrade()), "100");
    }
    /**
     * Tests saving users grades and retrieving them
     * Method: setUserEasyGrade(String conversionType, User user, String grade)
     *
     */
    @Test
    public void testSaveUserGrades() throws Exception {
        user.setBinaryDecimalEasyGrade(100);
        user.setBinaryDecimalMediumGrade(100);
        user.setBinaryDecimalHardGrade(100);
        assertEquals(String.valueOf(user.getBinaryDecimalEasyGrade()), "100");
        assertEquals(String.valueOf(user.getBinaryDecimalMediumGrade()), "100");
        assertEquals(String.valueOf(user.getBinaryDecimalHardGrade()), "100");

        user.setDecimalBinaryEasyGrade(100);
        user.setDecimalBinaryMediumGrade(100);
        user.setDecimalBinaryHardGrade(100);
        assertEquals(String.valueOf(user.getDecimalBinaryEasyGrade()), "100");
        assertEquals(String.valueOf(user.getDecimalBinaryMediumGrade()), "100");
        assertEquals(String.valueOf(user.getDecimalBinaryHardGrade()), "100");

        user.setDecimalHexadecimalEasyGrade(100);
        user.setDecimalHexadecimalMediumGrade(100);
        user.setDecimalHexadecimalHardGrade(100);
        assertEquals(String.valueOf(user.getDecimalHexadecimalEasyGrade()), "100");
        assertEquals(String.valueOf(user.getDecimalHexadecimalMediumGrade()), "100");
        assertEquals(String.valueOf(user.getDecimalHexadecimalHardGrade()), "100");

        user.setHexadecimalDecimalEasyGrade(100);
        user.setHexadecimalDecimalMediumGrade(100);
        user.setHexadecimalDecimalHardGrade(100);
        assertEquals(String.valueOf(user.getHexadecimalDecimalEasyGrade()), "100");
        assertEquals(String.valueOf(user.getHexadecimalDecimalMediumGrade()), "100");
        assertEquals(String.valueOf(user.getHexadecimalDecimalHardGrade()), "100");
    }

    /**
     * Tests saving users grades of 0
     * Method: setUserEasyGrade(String conversionType, User user, String grade)
     *
     */
    @Test
    public void testSaveUserGrades0() throws Exception {
        user.setBinaryDecimalEasyGrade(0);
        user.setBinaryDecimalMediumGrade(0);
        user.setBinaryDecimalHardGrade(0);
        assertEquals(String.valueOf(user.getBinaryDecimalEasyGrade()), "0");
        assertEquals(String.valueOf(user.getBinaryDecimalMediumGrade()), "0");
        assertEquals(String.valueOf(user.getBinaryDecimalHardGrade()), "0");

        user.setDecimalBinaryEasyGrade(0);
        user.setDecimalBinaryMediumGrade(0);
        user.setDecimalBinaryHardGrade(0);
        assertEquals(String.valueOf(user.getDecimalBinaryEasyGrade()), "0");
        assertEquals(String.valueOf(user.getDecimalBinaryMediumGrade()), "0");
        assertEquals(String.valueOf(user.getDecimalBinaryHardGrade()), "0");

        user.setDecimalHexadecimalEasyGrade(0);
        user.setDecimalHexadecimalMediumGrade(0);
        user.setDecimalHexadecimalHardGrade(0);
        assertEquals(String.valueOf(user.getDecimalHexadecimalEasyGrade()), "0");
        assertEquals(String.valueOf(user.getDecimalHexadecimalMediumGrade()), "0");
        assertEquals(String.valueOf(user.getDecimalHexadecimalHardGrade()), "0");

        user.setHexadecimalDecimalEasyGrade(0);
        user.setHexadecimalDecimalMediumGrade(0);
        user.setHexadecimalDecimalHardGrade(0);
        assertEquals(String.valueOf(user.getHexadecimalDecimalEasyGrade()), "0");
        assertEquals(String.valueOf(user.getHexadecimalDecimalMediumGrade()), "0");
        assertEquals(String.valueOf(user.getHexadecimalDecimalHardGrade()), "0");
    }

    /**
     * Tests users taking same exam more than once
     * Method: setUserEasyGrade(String conversionType, User user, String grade)
     *
     */
    @Test
    public void testSaveUserGradesUpdateSameExam() throws Exception {
        user.setBinaryDecimalEasyGrade(50);
        assertEquals(String.valueOf(user.getBinaryDecimalEasyGrade()), "50");
        user.setBinaryDecimalEasyGrade(80);
        assertEquals(String.valueOf(user.getBinaryDecimalEasyGrade()), "80");
    }
}
