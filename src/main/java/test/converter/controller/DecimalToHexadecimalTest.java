package test.converter.controller;

import converter.conversion.DecimalToHexadecimal;
import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DecimalToHexadecimalTest {

    /**
     * Tests exam difficulty level easy
     * Method: randomDecimalString(Integer difficulty_int)
     *
     */
    @Test
    public void testDecimalToHexadecimalDifficultyEasy(){
        for (int i = 0; i <100; i++) {
            String number = new DecimalToHexadecimal().randomDecimalString(15);
            assertTrue(Integer.toBinaryString(Integer.parseInt(number)).length()>= Integer.toBinaryString(7).length()-2);
        }
    }
    /**
     * Tests exam difficulty level medium
     * Method: randomDecimalString(Integer difficulty_int)
     *
     */
    @Test
    public void testDecimalToHexadecimalDifficultyMedium(){
        for (int i = 0; i <100; i++) {
            String number = new DecimalToHexadecimal().randomDecimalString(31);
            assertTrue(Integer.toBinaryString(Integer.parseInt(number)).length()>= Integer.toBinaryString(31).length()-2);
        }
    }
    /**
     * Tests exam difficulty level hard
     * Method: randomDecimalString(Integer difficulty_int)
     *
     */
    @Test
    public void testDecimalToHexadecimalNumberHard() {
        for (int i = 0; i < 100; i++) {
            String number = new DecimalToHexadecimal().randomDecimalString(1027);
            assertTrue(Integer.toBinaryString(Integer.parseInt(number)).length()>= Integer.toBinaryString(1027).length()-2);
        }
    }

    /**
     * Tests users input correct answer 100 times
     * Method: calculateAnswer(Integer difficulty_int)
     *
     */
    @Test
    public void testDecimalToHexadecimalOneCorrectAnswer100Times() {
        for (int i = 0; i < 100; i++) {
            assertTrue(Objects.equals(new DecimalToHexadecimal().calculateAnswer("A", "10"), "Correct!"));
        }
    }

    /**
     * Tests users input incorrect answer 100 times
     * Method: calculateAnswer(Integer difficulty_int)
     *
     */
    @Test
    public void testDecimalToHexadecimalOneIncorrectAnswer100Times() {
        for (int i = 0; i < 100; i++) {
            assertTrue(Objects.equals(new DecimalToHexadecimal().calculateAnswer("B", "10"), "Incorrect!"));
        }
    }

    /**
     * Tests users input 100 correct answer
     * Method: calculateAnswer(Integer difficulty_int)
     *
     */
    @Test
    public void testDecimalToHexadecimal100IncorrectAnswers() {
        for (int i = 0; i < 100; i++) {
            assertTrue(Objects.equals(new DecimalToHexadecimal().calculateAnswer(Integer.toHexString(i), Integer.toString(i+1)), "Incorrect!"));
        }
    }

    /**
     * Tests users input 100 incorrect answers
     * Method: calculateAnswer(Integer difficulty_int)
     *
     */
    @Test
    public void testDecimalToHexadecimal100CorrectAnswers() {
        for (int i = 0; i < 100; i++) {
            assertTrue(Objects.equals(new DecimalToHexadecimal().calculateAnswer(Integer.toHexString(i), Integer.toString(i)), "Correct!"));
        }
    }

    /**
     * Tests users input 0 incorrect answer//
     * Assert it is false
     * Method: calculateAnswer(Integer difficulty_int)
     *
     */
    @Test
    public void testDecimalToHexadecimal0() {
            assertFalse(Objects.equals(new DecimalToHexadecimal().calculateAnswer("0", "15"), "Correct!"));
    }
}
