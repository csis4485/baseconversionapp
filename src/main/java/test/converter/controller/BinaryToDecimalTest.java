package test.converter.controller; 

import converter.conversion.BinaryToDecimal;
import converter.model.BinaryToDecimalExam;
import converter.model.User;
import converter.service.GradeService;
import converter.service.UtilityService;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/** 
* BinaryToDecimalController Tester. 
* 
* @author <Authors name> 
* @since <pre>Dec 4, 2016</pre> 
* @version 1.0 
*/ 
public class BinaryToDecimalTest {
    private User user = new User();
    private GradeService gradeService = new GradeService();
    private ArrayList<String> easyTestRandomList = new BinaryToDecimalExam().getEASY_RANDOM_LIST();
    private ArrayList<String> mediumTestRandomList = new BinaryToDecimalExam().getMEDIUM_RANDOM_LIST();
    private ArrayList<String> hardTestRandomList = new BinaryToDecimalExam().getHARD_RANDOM_LIST();


    /**
     * Tests binary number format on easy exam
     * Method: randomBinaryString(Integer difficulty_int)
     *
     */
    @Test
    public void testBinaryRandomEasyNumberFormat(){
        String binaryNumber = new BinaryToDecimal().randomBinaryString(7);
        for (char i:binaryNumber.toCharArray()) {
            assertTrue(i=='0' ||i=='1');
        }
    }

    /**
     * Tests binary number format on medium exam
     * Method: randomBinaryString(Integer difficulty_int)
     *
     */
    @Test
    public void testBinaryRandomMediumNumberFormat(){
        String binaryNumber = new BinaryToDecimal().randomBinaryString(31);
        for (char i:binaryNumber.toCharArray()) {
            assertTrue(i=='0' ||i=='1');
        }
    }
    /**
     * Tests binary number format on hard exam
     * Method: randomBinaryString(Integer difficulty_int)
     *
     */
    @Test
    public void testBinaryRandomHardNumberFormat(){
        String binaryNumber = new BinaryToDecimal().randomBinaryString(1023);
        for (char i:binaryNumber.toCharArray()) {
            assertTrue(i=='0' ||i=='1');
        }
    }
    /**
     * Tests easy exam question difficulty level
     * Method: randomBinaryString(Integer difficulty_int)
     *
     */
    @Test
    public void testBinaryToDecimalDifficultyEasy(){
        for (int i = 0; i <100; i++) {
            for (int j = 0; j<5 ; j++) {
                String binaryNumber = new BinaryToDecimal().randomBinaryString(7);
                assertTrue(binaryNumber.charAt(j)=='0');
            }
        }
    }
    /**
     * Tests medium exam question difficulty level
     * Method: randomBinaryString(Integer difficulty_int)
     *
     */
    @Test
    public void testBinaryToDecimalDifficultyMedium(){
        for (int i = 0; i <100; i++) {
            for (int j = 0; j<3 ; j++) {
                String binaryNumber = new BinaryToDecimal().randomBinaryString(31);
                assertTrue(binaryNumber.charAt(j)=='0');
            }
        }
    }
    /**
     * Tests hard exam question difficulty level
     * Method: randomBinaryString(Integer difficulty_int)
     *
     */
    @Test
    public void testBinaryToDecimalBinaryNumberHard() {
        for (int i = 0; i < 100; i++) {
            String binaryNumber = new BinaryToDecimal().randomBinaryString(1027);
            assertTrue(binaryNumber.charAt(0) == '1');
        }
    }
    /**
    * Tests 5 correct answers on easy exam
    * Method: binaryToDecimalTest(@ModelAttribute("user") User user, Model model)
    *
    */
    @Test
    public void testBinaryToDecimalEasyExamAllCorrect() throws Exception {
        user.setEasyAnswers(easyTestRandomList);
        gradeService.setUserEasyGrade("binaryToDecimal",user, String.valueOf(gradeService.setGrade( user,"binaryToDecimal", easyTestRandomList, true, false, false)));
        assertEquals(String.valueOf(user.getBinaryDecimalEasyGrade()), "100");
    }

    /**
     * Tests 5 incorrect answers on easy exam
     * Method: binaryToDecimalTest(@ModelAttribute("user") User user, Model model)
     *
     */
    @Test
    public void testBinaryToDecimalEasyExamAllIncorrect() throws Exception {
        user.setEasyAnswers(new UtilityService().createStringArray(
                "0","0","0","0","0"
        ));
        gradeService.setUserEasyGrade("binaryToDecimal",user, String.valueOf(gradeService.setGrade( user,"binaryToDecimal", easyTestRandomList, true, false, false)));
        assertEquals(String.valueOf(user.getBinaryDecimalEasyGrade()), "0");
    }

    /**
     * Tests 3 correct answers on easy exam
     * Method: binaryToDecimalTest(@ModelAttribute("user") User user, Model model)
     *
     */
    @Test
    public void testBinaryToDecimalEasyExam3Correct() throws Exception {
        user.setEasyAnswers(new UtilityService().createStringArray(
                String.valueOf(Integer.parseInt(easyTestRandomList.get(0), 2)),
                "0",
                String.valueOf(Integer.parseInt(easyTestRandomList.get(2), 2)),
                "0",
                String.valueOf(Integer.parseInt(easyTestRandomList.get(4), 2))
        ));
        gradeService.setUserEasyGrade("binaryToDecimal",user, String.valueOf(gradeService.setGrade( user,"binaryToDecimal", easyTestRandomList, true, false, false)));
        assertEquals(String.valueOf(user.getBinaryDecimalEasyGrade()), "60");
    }

    /**
     * Tests 5 correct answers on medium exam
     * Method: postBinaryToDecimalExam(@ModelAttribute("user") User user, Model model)
     *
     */
    @Test
    public void testBinaryToDecimalMediumExamAllCorrect() throws Exception {
        user.setMediumAnswers(mediumTestRandomList);
        gradeService.setUserMediumGrade("binaryToDecimal",user, String.valueOf(gradeService.setGrade( user,"binaryToDecimal", mediumTestRandomList, false, true, false)));
        assertEquals(String.valueOf(user.getBinaryDecimalMediumGrade()), "100");
    }

    /**
     * Tests 5 incorrect answers on medium exam
     * Method: binaryToDecimalTest(@ModelAttribute("user") User user, Model model)
     *
     */
    @Test
    public void testBinaryToDecimalMediumExamAllIncorrect() throws Exception {
        user.setMediumAnswers(new UtilityService().createStringArray(
                "0","0","0","0","0"
        ));
        gradeService.setUserMediumGrade("binaryToDecimal",user, String.valueOf(gradeService.setGrade( user,"binaryToDecimal", mediumTestRandomList, false, true, false)));
        assertEquals(String.valueOf(user.getBinaryDecimalMediumGrade()), "0");
    }

    /**
     * Tests 3 correct answers on medium exam
     * Method: postBinaryToDecimalExam(@ModelAttribute("user") User user, Model model)
     *
     */
    @Test
    public void testBinaryToDecimalMediumExam3Correct() throws Exception {
        user.setMediumAnswers(new UtilityService().createStringArray(
                String.valueOf(Integer.parseInt(mediumTestRandomList.get(0), 2)),
                "0",
                String.valueOf(Integer.parseInt(mediumTestRandomList.get(2), 2)),
                "0",
                String.valueOf(Integer.parseInt(mediumTestRandomList.get(4), 2))
        ));
        gradeService.setUserMediumGrade("binaryToDecimal",user, String.valueOf(gradeService.setGrade( user,"binaryToDecimal", mediumTestRandomList, false, true, false)));
        assertEquals(String.valueOf(user.getBinaryDecimalMediumGrade()), "60");
    }

    /**
     * Tests 5 correct answers on hard exam
     * Method: binaryToDecimalTest(@ModelAttribute("user") User user, Model model)
     *
     */
    @Test
    public void testBinaryToDecimalHardExamAllCorrect() throws Exception {
        user.setHardAnswers(hardTestRandomList);
        gradeService.setUserHardGrade("binaryToDecimal",user, String.valueOf(gradeService.setGrade( user,"binaryToDecimal", hardTestRandomList, false, false, true)));
        assertEquals(String.valueOf(user.getBinaryDecimalHardGrade()), "100");
    }

    /**
     * Tests 5 incorrect answers on hard exam
     * Method: binaryToDecimalTest(@ModelAttribute("user") User user, Model model)
     *
     */
    @Test
    public void testBinaryToDecimalHardExamAllIncorrect() throws Exception {
        user.setHardAnswers(new UtilityService().createStringArray(
                "0","0","0","0","0"
        ));
        gradeService.setUserHardGrade("binaryToDecimal",user, String.valueOf(gradeService.setGrade( user,"binaryToDecimal", hardTestRandomList, false, false, true)));
        assertEquals(String.valueOf(user.getBinaryDecimalHardGrade()), "0");
    }

    /**
     * Tests 3 correct answers on hard exam
     * Method: binaryToDecimalTest(@ModelAttribute("user") User user, Model model)
     *
     */
    @Test
    public void testBinaryToDecimalHardExam3Correct() throws Exception {
        user.setHardAnswers(new UtilityService().createStringArray(
                String.valueOf(Integer.parseInt(hardTestRandomList.get(0), 2)),
                "0",
                String.valueOf(Integer.parseInt(hardTestRandomList.get(2), 2)),
                "0",
                String.valueOf(Integer.parseInt(hardTestRandomList.get(4), 2))
        ));
        gradeService.setUserHardGrade("binaryToDecimal",user, String.valueOf(gradeService.setGrade( user,"binaryToDecimal", hardTestRandomList, false, false, true)));
        assertEquals(String.valueOf(user.getBinaryDecimalHardGrade()), "60");
    }
} 
