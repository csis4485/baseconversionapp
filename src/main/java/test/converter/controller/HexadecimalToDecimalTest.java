package test.converter.controller;


import converter.conversion.HexadecimalToDecimal;
import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class HexadecimalToDecimalTest {

    /**
     * Tests exam difficulty level easy
     * Method: randomHexadecimalString(Integer difficulty_int)
     *
     */
    @Test
    public void testHexadecimalToDecimalDifficultyEasy(){
        for (int i = 0; i <100; i++) {
            String number = new HexadecimalToDecimal().randomHexadecimalString(15);
            assertTrue(Integer.toBinaryString(Integer.parseInt(number,16)).length()>= Integer.toBinaryString(7).length()-2);
        }
    }
    /**
     * Tests exam difficulty level medium
     * Method: randomHexadecimalString(Integer difficulty_int)
     *
     */
    @Test
    public void testHexadecimalToDecimalDifficultyMedium(){
        for (int i = 0; i <100; i++) {
            String number = new HexadecimalToDecimal().randomHexadecimalString(31);
            assertTrue(Integer.toBinaryString(Integer.parseInt(number,16)).length()>= Integer.toBinaryString(31).length()-2);
        }
    }
    /**
     * Tests exam difficulty level hard
     * Method: randomHexadecimalString(Integer difficulty_int)
     *
     */
    @Test
    public void testHexadecimalToDecimalNumberHard() {
        for (int i = 0; i < 100; i++) {
            String number = new HexadecimalToDecimal().randomHexadecimalString(1027);
            assertTrue(Integer.toBinaryString(Integer.parseInt(number,16)).length()>= Integer.toBinaryString(1027).length()-2);
        }
    }

    /**
     * Tests users input correct answer 100 times
     * Method: calculateAnswer(Integer difficulty_int)
     *
     */
    @Test
    public void testHexadecimalToDecimalOneCorrectAnswer100Times() {
        for (int i = 0; i < 100; i++) {
            assertTrue(Objects.equals(new HexadecimalToDecimal().calculateAnswer("10", "A"), "Correct!"));
        }
    }

    /**
     * Tests users input incorrect answer 100 times
     * Method: calculateAnswer(Integer difficulty_int)
     *
     */
    @Test
    public void testHexadecimalToDecimalOneIncorrectAnswer100Times() {
        for (int i = 0; i < 100; i++) {
            assertTrue(Objects.equals(new HexadecimalToDecimal().calculateAnswer("10", "B"), "Incorrect!"));
        }
    }

    /**
     * Tests users input 100 correct answer
     * Method: calculateAnswer(Integer difficulty_int)
     *
     */
    @Test
    public void testHexadecimalToDecimal100IncorrectAnswers() {
        for (int i = 0; i < 100; i++) {
            assertTrue(Objects.equals(new HexadecimalToDecimal().calculateAnswer(Integer.toString(i), Integer.toHexString(i+1)), "Incorrect!"));
        }
    }

    /**
     * Tests users input 100 incorrect answers
     * Method: calculateAnswer(Integer difficulty_int)
     *
     */
    @Test
    public void testHexadecimalToDecimal100CorrectAnswers() {
        for (int i = 0; i < 100; i++) {
            assertTrue(Objects.equals(new HexadecimalToDecimal().calculateAnswer(Integer.toString(i), Integer.toHexString(i)), "Correct!"));
        }
    }

    /**
     * Tests users input 0 incorrect answer//
     * Assert it is false
     * Method: calculateAnswer(Integer difficulty_int)
     *
     */
    @Test
    public void testHexadecimalToDecimal0() {
        assertFalse(Objects.equals(new HexadecimalToDecimal().calculateAnswer("0", "B"), "Correct!"));
    }
}
