package test.converter.controller;


import converter.conversion.DecimalToBinary;
import converter.model.DecimalToBinaryExam;
import converter.model.User;
import converter.service.GradeService;
import converter.service.UtilityService;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DecimalToBinaryTest {
    private User user = new User();
    private GradeService gradeService = new GradeService();
    private ArrayList<String> easyTestRandomList = new DecimalToBinaryExam().getEASY_RANDOM_LIST();
    private String easy1 = Integer.toBinaryString(Integer.parseInt(easyTestRandomList.get(0))).replace(' ', '0');
    private String easy2 = Integer.toBinaryString(Integer.parseInt(easyTestRandomList.get(1))).replace(' ', '0');
    private String easy3 = Integer.toBinaryString(Integer.parseInt(easyTestRandomList.get(2))).replace(' ', '0');
    private String easy4 = Integer.toBinaryString(Integer.parseInt(easyTestRandomList.get(3))).replace(' ', '0');
    private String easy5 = Integer.toBinaryString(Integer.parseInt(easyTestRandomList.get(4))).replace(' ', '0');
    private ArrayList<String> mediumTestRandomList = new DecimalToBinaryExam().getMEDIUM_RANDOM_LIST();
    private String medium1 = Integer.toBinaryString(Integer.parseInt(mediumTestRandomList.get(0))).replace(' ', '0');
    private String medium2 = Integer.toBinaryString(Integer.parseInt(mediumTestRandomList.get(1))).replace(' ', '0');
    private String medium3 = Integer.toBinaryString(Integer.parseInt(mediumTestRandomList.get(2))).replace(' ', '0');
    private String medium4 = Integer.toBinaryString(Integer.parseInt(mediumTestRandomList.get(3))).replace(' ', '0');
    private String medium5 = Integer.toBinaryString(Integer.parseInt(mediumTestRandomList.get(4))).replace(' ', '0');
    private ArrayList<String> hardTestRandomList = new DecimalToBinaryExam().getHARD_RANDOM_LIST();
    private String hard1 = Integer.toBinaryString(Integer.parseInt(hardTestRandomList.get(0))).replace(' ', '0');
    private String hard2 = Integer.toBinaryString(Integer.parseInt(hardTestRandomList.get(1))).replace(' ', '0');
    private String hard3 = Integer.toBinaryString(Integer.parseInt(hardTestRandomList.get(2))).replace(' ', '0');
    private String hard4 = Integer.toBinaryString(Integer.parseInt(hardTestRandomList.get(3))).replace(' ', '0');
    private String hard5 = Integer.toBinaryString(Integer.parseInt(hardTestRandomList.get(4))).replace(' ', '0');


    /**
     * Tests 100 exam questions on easy exam to ensure difficulty level
     * Method: randomDecimalString(Integer difficulty_int)
     *
     */
    @Test
    public void testDecimalToBinaryDifficultyEasy(){
        for (int i = 0; i <100; i++) {
            String number = new DecimalToBinary().randomDecimalString(7);
            assertTrue(Integer.toBinaryString(Integer.parseInt(number)).length()>= Integer.toBinaryString(7).length()-2);
        }
    }
    /**
     * Tests 100 exam questions on medium exam to ensure difficulty level
     * Method: randomDecimalString(Integer difficulty_int)
     *
     */
    @Test
    public void testDecimalToBinaryDifficultyMedium(){
        for (int i = 0; i <100; i++) {
            String number = new DecimalToBinary().randomDecimalString(31);
            assertTrue(Integer.toBinaryString(Integer.parseInt(number)).length()>= Integer.toBinaryString(31).length()-2);
        }
    }
    /**
     * Tests 100 exam questions on hard exam to ensure difficulty level
     * Method: randomDecimalString(Integer difficulty_int)
     *
     */
    @Test
    public void testDecimalToBinaryNumberHard() {
        for (int i = 0; i < 100; i++) {
            String number = new DecimalToBinary().randomDecimalString(1027);
            assertTrue(Integer.toBinaryString(Integer.parseInt(number)).length()>= Integer.toBinaryString(1027).length()-2);
        }
    }

    /**
     * Tests 5 correct answers on easy exam
     * Method: binaryToDecimalTest(@ModelAttribute("user") User user, Model model)
     *
     */
    @Test
    public void testDecimalToBinaryEasyExamAllCorrect() throws Exception {
        user.setEasyAnswers(new UtilityService().createStringArray(
                easy1,
                easy2,
                easy3,
                easy4,
                easy5
        ));
        gradeService.setUserEasyGrade("decimalToBinary",user, String.valueOf(gradeService.setGrade( user,"decimalToBinary", easyTestRandomList, true, false, false)));
        assertEquals(String.valueOf(user.getDecimalBinaryEasyGrade()), "100");
    }

    /**
     * Tests 5 incorrect answers on easy exam
     * Method: binaryToDecimalTest(@ModelAttribute("user") User user, Model model)
     *
     */
    @Test
    public void testDecimalToBinaryEasyExamAllIncorrect() throws Exception {
        user.setEasyAnswers(new UtilityService().createStringArray(
                "0","0","0","0","0"
        ));
        gradeService.setUserEasyGrade("decimalToBinary", user, String.valueOf(gradeService.setGrade( user,"decimalToBinary", easyTestRandomList, true, false, false)));
        assertEquals(String.valueOf(user.getDecimalBinaryEasyGrade()), "0");
    }

    /**
     * Tests 3 correct answers on easy exam
     * Method: binaryToDecimalTest(@ModelAttribute("user") User user, Model model)
     *
     */
    @Test
    public void testDecimalToBinaryEasyExam3Correct() throws Exception {
        user.setEasyAnswers(new UtilityService().createStringArray(
                easy1,
                "0",
                easy3,
                "0",
                easy5
        ));
        gradeService.setUserEasyGrade("decimalToBinary",user, String.valueOf(gradeService.setGrade( user,"decimalToBinary", easyTestRandomList, true, false, false)));
        assertEquals(String.valueOf(user.getDecimalBinaryEasyGrade()), "60");
    }

    /**
     * Tests 5 correct answers on medium exam
     * Method: postBinaryToDecimalExam(@ModelAttribute("user") User user, Model model)
     *
     */
    @Test
    public void testDecimalToBinaryMediumExamAllCorrect() throws Exception {
        user.setMediumAnswers(new UtilityService().createStringArray(
                medium1,
                medium2,
                medium3,
                medium4,
                medium5
        ));
        gradeService.setUserMediumGrade("decimalToBinary",user, String.valueOf(gradeService.setGrade( user,"decimalToBinary", mediumTestRandomList, false, true, false)));
        assertEquals(String.valueOf(user.getDecimalBinaryMediumGrade()), "100");
    }

    /**
     * Tests 5 incorrect answers on medium exam
     * Method: binaryToDecimalTest(@ModelAttribute("user") User user, Model model)
     *
     */
    @Test
    public void testDecimalToBinaryMediumExamAllIncorrect() throws Exception {
        user.setMediumAnswers(new UtilityService().createStringArray(
                "0","0","0","0","0"
        ));
        gradeService.setUserMediumGrade("decimalToBinary",user, String.valueOf(gradeService.setGrade( user,"decimalToBinary", mediumTestRandomList, false, true, false)));
        assertEquals(String.valueOf(user.getDecimalBinaryMediumGrade()), "0");
    }

    /**
     * Tests 3 correct answers on medium exam
     * Method: postBinaryToDecimalExam(@ModelAttribute("user") User user, Model model)
     *
     */
    @Test
    public void testDecimalToBinaryMediumExam3Correct() throws Exception {
        user.setMediumAnswers(new UtilityService().createStringArray(
                medium1,
                "0",
                medium3,
                "0",
                medium5
        ));
        gradeService.setUserMediumGrade("decimalToBinary",user, String.valueOf(gradeService.setGrade( user,"decimalToBinary", mediumTestRandomList, false, true, false)));
        assertEquals(String.valueOf(user.getDecimalBinaryMediumGrade()), "60");
    }

    /**
     * Tests 5 correct answers on hard exam
     * Method: binaryToDecimalTest(@ModelAttribute("user") User user, Model model)
     *
     */
    @Test
    public void testDecimalToBinaryHardExamAllCorrect() throws Exception {
        user.setHardAnswers(new UtilityService().createStringArray(
                hard1,
                hard2,
                hard3,
                hard4,
                hard5
        ));
        gradeService.setUserHardGrade("decimalToBinary",user, String.valueOf(gradeService.setGrade( user,"decimalToBinary", hardTestRandomList, false, false, true)));
        assertEquals(String.valueOf(user.getDecimalBinaryHardGrade()), "100");
    }

    /**
     * Tests 5 incorrect answers on hard exam
     * Method: binaryToDecimalTest(@ModelAttribute("user") User user, Model model)
     *
     */
    @Test
    public void testDecimalToBinaryHardExamAllIncorrect() throws Exception {
        user.setHardAnswers(new UtilityService().createStringArray(
                "0","0","0","0","0"
        ));
        gradeService.setUserHardGrade("decimalToBinary",user, String.valueOf(gradeService.setGrade( user,"decimalToBinary", hardTestRandomList, false, false, true)));
        assertEquals(String.valueOf(user.getDecimalBinaryHardGrade()), "0");
    }

    /**
     * Tests 3 correct answers on hard exam
     * Method: binaryToDecimalTest(@ModelAttribute("user") User user, Model model)
     *
     */
    @Test
    public void testDecimalToBinaryHardExam3Correct() throws Exception {
        user.setHardAnswers(new UtilityService().createStringArray(
                hard1,
                "0",
                hard3,
                "0",
                hard5
        ));
        gradeService.setUserHardGrade("decimalToBinary",user, String.valueOf(gradeService.setGrade( user,"decimalToBinary", hardTestRandomList, false, false, true)));
        assertEquals(String.valueOf(user.getDecimalBinaryHardGrade()), "60");
    }
}
