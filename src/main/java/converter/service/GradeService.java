package converter.service;

import converter.model.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Objects;

public class GradeService extends UtilityService{

    @Autowired
    private static ArrayList<String> resultArray = new ArrayList<>();

    /**
     * Method to set the users grade upon submitting current exam answers
     *
     * @param user = logged in user
     * @param easy = true if difficulty level is easy, false otherwise
     * @param medium = true if difficulty level is medium, false otherwise
     * @param hard = true if difficulty level is hard, false otherwise
     */
    public Integer setGrade(User user, String conversionType, ArrayList<String> randomList, boolean easy, boolean medium, boolean hard) {

        if(easy){
            Integer GRADE = 0;
            resultArray = new UtilityService().calculateResults(user.getEasyAnswers(),randomList,conversionType);
            for (String res : resultArray) {
                if(Objects.equals(res, "Correct!")){
                    GRADE = GRADE+1;
                }
            }
            return GRADE*20;
        }

        if(medium){
            Integer GRADE = 0;
            resultArray = new UtilityService().calculateResults(user.getMediumAnswers(),randomList,conversionType);
            for (String res : resultArray) {
                if(Objects.equals(res, "Correct!")){
                    GRADE = GRADE+1;
                }
            }
            return GRADE*20;

        }

        if(hard){
            Integer GRADE = 0;
            resultArray = new UtilityService().calculateResults(user.getHardAnswers(),randomList,conversionType);
            for (String res : resultArray) {
                if(Objects.equals(res, "Correct!")){
                    GRADE = GRADE+1;
                }
            }
            return GRADE*20;
        }
        return 0;
    }

    public void setUserEasyGrade(String conversionType, User user, String grade) {
        switch (conversionType) {
            case "binaryToDecimal":
                user.setBinaryDecimalEasyGrade(Integer.valueOf(grade));
            case "decimalToBinary":
                user.setDecimalBinaryEasyGrade(Integer.valueOf(grade));
            case "decimalToHexadecimal":
                user.setDecimalHexadecimalEasyGrade(Integer.valueOf(grade));

            case "hexadecimalToDecimal":
                user.setHexadecimalDecimalEasyGrade(Integer.valueOf(grade));
        }
    }
    public void setUserMediumGrade(String conversionType, User user, String grade) {
        switch (conversionType) {
            case "binaryToDecimal":
                user.setBinaryDecimalMediumGrade(Integer.valueOf(grade));
            case "decimalToBinary":
                user.setDecimalBinaryMediumGrade(Integer.valueOf(grade));
            case "decimalToHexadecimal":
                user.setDecimalHexadecimalMediumGrade(Integer.valueOf(grade));
            case "hexadecimalToDecimal":
                user.setHexadecimalDecimalMediumGrade(Integer.valueOf(grade));
        }
    }
    public void setUserHardGrade(String conversionType, User user, String grade) {
        switch (conversionType) {
            case "binaryToDecimal":
                user.setBinaryDecimalHardGrade(Integer.valueOf(grade));
            case "decimalToBinary":
                user.setDecimalBinaryHardGrade(Integer.valueOf(grade));
            case "decimalToHexadecimal":
                user.setDecimalHexadecimalHardGrade(Integer.valueOf(grade));
            case "hexadecimalToDecimal":
                user.setHexadecimalDecimalHardGrade(Integer.valueOf(grade));
        }
    }

}
