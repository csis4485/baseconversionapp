package converter.service;

import converter.conversion.BinaryToDecimal;
import converter.conversion.DecimalToBinary;
import converter.conversion.DecimalToHexadecimal;
import converter.conversion.HexadecimalToDecimal;

import java.util.ArrayList;

public class UtilityService {


    private BinaryToDecimal binaryToDecimal = new BinaryToDecimal();
    private DecimalToBinary decimalToBinary = new DecimalToBinary();
    private DecimalToHexadecimal decimalToHexadecimal = new DecimalToHexadecimal();
    private HexadecimalToDecimal hexadecimalToDecimal = new HexadecimalToDecimal();
    private ArrayList<String> resultArray = new ArrayList<>();
    private String result1, result2, result3,result4,result5;

    /**
     * Method to calculate results from users answers.  Uses a switch statement based on the different types of conversions
     * @param userAnswers = Array of answers from users input
     * @param randomList = Array of correct answers
     * @param conversionType = String type of conversion for switch statement cases
     * @return String Array of "Correct!" or "Incorrect" results
     */
    public ArrayList<String> calculateResults(ArrayList<String> userAnswers, ArrayList<String> randomList, String conversionType) {
        switch (conversionType) {
            case "binaryToDecimal":
                result1 = binaryToDecimal.calculateAnswer(userAnswers.get(0), randomList.get(0));
                result2 = binaryToDecimal.calculateAnswer(userAnswers.get(1), randomList.get(1));
                result3 = binaryToDecimal.calculateAnswer(userAnswers.get(2), randomList.get(2));
                result4 = binaryToDecimal.calculateAnswer(userAnswers.get(3), randomList.get(3));
                result5 = binaryToDecimal.calculateAnswer(userAnswers.get(4), randomList.get(4));
                return resultArray = createStringArray(result1, result2, result3, result4, result5);

            case "decimalToBinary":
                result1 = decimalToBinary.calculateAnswer(userAnswers.get(0), randomList.get(0));
                result2 = decimalToBinary.calculateAnswer(userAnswers.get(1), randomList.get(1));
                result3 = decimalToBinary.calculateAnswer(userAnswers.get(2), randomList.get(2));
                result4 = decimalToBinary.calculateAnswer(userAnswers.get(3), randomList.get(3));
                result5 = decimalToBinary.calculateAnswer(userAnswers.get(4), randomList.get(4));
                return resultArray = createStringArray(result1, result2, result3, result4, result5);
            case "decimalToHexadecimal":
                result1 = decimalToHexadecimal.calculateAnswer(userAnswers.get(0), randomList.get(0));
                result2 = decimalToHexadecimal.calculateAnswer(userAnswers.get(1), randomList.get(1));
                result3 = decimalToHexadecimal.calculateAnswer(userAnswers.get(2), randomList.get(2));
                result4 = decimalToHexadecimal.calculateAnswer(userAnswers.get(3), randomList.get(3));
                result5 = decimalToHexadecimal.calculateAnswer(userAnswers.get(4), randomList.get(4));
                return resultArray = createStringArray(result1, result2, result3, result4, result5);
            case "hexadecimalToDecimal":
                result1 = hexadecimalToDecimal.calculateAnswer(userAnswers.get(0), randomList.get(0));
                result2 = hexadecimalToDecimal.calculateAnswer(userAnswers.get(1), randomList.get(1));
                result3 = hexadecimalToDecimal.calculateAnswer(userAnswers.get(2), randomList.get(2));
                result4 = hexadecimalToDecimal.calculateAnswer(userAnswers.get(3), randomList.get(3));
                result5 = hexadecimalToDecimal.calculateAnswer(userAnswers.get(4), randomList.get(4));
                return resultArray = createStringArray(result1, result2, result3, result4, result5);

        }
        return resultArray;
    }


    /**
     *
     * @param s1 = results from calculating users answer to Question 1
     * @param s2 = results from calculating users answer to Question 2
     * @param s3 = results from calculating users answer to Question 3
     * @param s4 = results from calculating users answer to Question 4
     * @param s5 = results from calculating users answer to Question 5
     * @return ArrayList of String "results" from comparing the users answer and the correct answer.
     */
    public ArrayList<String> createStringArray(final String s1, final String s2, final String s3, final String s4, final String s5) {
        return new ArrayList<String>(){{
            add(s1);
            add(s2);
            add(s3);
            add(s4);
            add(s5);}};
    }
}
