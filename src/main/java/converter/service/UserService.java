package converter.service;


import converter.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
