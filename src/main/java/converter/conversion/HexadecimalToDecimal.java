package converter.conversion;

import java.util.Random;


/**
 * Converts Hexadecimal numbers to decimal
 */
public class HexadecimalToDecimal {


    public String randomHexadecimalString(Integer difficulty_int){
        Random rand = new Random();
        Integer number = rand.nextInt(difficulty_int) ;
        while (Integer.toBinaryString(number).length() < Integer.toBinaryString(difficulty_int).length()-2){
            number=rand.nextInt(difficulty_int);
        }
        return Integer.toHexString(number);
    }
    /**
     * Compares users input with correct answer
     *
     * @param userDecimalString = users input
     * @param correctAnswer = correct answer
     * @return "Correct!", if users input is the right answer
     *         "Incorrect!", if users input is not the right answer
     */
    public String calculateAnswer(String userDecimalString, String correctAnswer)
    {
        if (correctAnswer.equalsIgnoreCase(Integer.toHexString(Integer.valueOf(userDecimalString)))) {
            return "Correct!";
        }
        else {
            return "Incorrect!";
        }
    }
}
