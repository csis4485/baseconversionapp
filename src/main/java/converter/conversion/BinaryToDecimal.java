package converter.conversion;

import java.util.Objects;
import java.util.Random;
/**
 * Converts binary numbers to decimal
 */
public class BinaryToDecimal {

    public String randomBinaryString(Integer difficulty_int){

        Random rand = new Random();
        Integer binaryNumber = rand.nextInt(difficulty_int) ;
        while (Integer.toBinaryString(binaryNumber).length()< Integer.toBinaryString(difficulty_int).length()-2){
            binaryNumber=rand.nextInt(difficulty_int);
        }
        return String.format("%8s", Integer.toBinaryString(binaryNumber)).replace(' ', '0');
    }

    /**
     * Compares users input with correct answer
     *
     * @param userIntegerString = users input
     * @param correctAnswer = correct answer
     * @return "Correct!", if users input is the right answer
     *         "Incorrect!", if users input is not the right answer
     */
    public String calculateAnswer(String userIntegerString, String correctAnswer)
    {
        if(Objects.equals(userIntegerString, "0")){
            return "Incorrect";
        }

        if(userIntegerString.length()<8) {
            userIntegerString = String.format("%8s", Integer.toBinaryString(Integer.parseInt(userIntegerString))).replace(' ', '0');
        }
        if (Objects.equals( userIntegerString , correctAnswer )) {
            return "Correct!";
        }
        else {
            return "Incorrect!";
        }
    }
}