package converter.conversion;

import java.util.Objects;
import java.util.Random;

/**
 * Converts decimal numbers to hexadecimal
 */
public class DecimalToHexadecimal {


    public String randomDecimalString(Integer difficulty_int){
        Random rand = new Random();
        Integer decimalNumber = rand.nextInt(difficulty_int) ;
        while (Integer.toBinaryString(decimalNumber).length() < Integer.toBinaryString(difficulty_int).length()-2){
            decimalNumber=rand.nextInt(difficulty_int);
        }
        return String.valueOf(decimalNumber);
    }
    /**
     * Compares users input with correct answer
     *
     * @param userHexString = users input
     * @param correctAnswer = correct answer
     * @return "Correct!", if users input is the right answer
     *         "Incorrect!", if users input is not the right answer
     */
    public String calculateAnswer(String userHexString, String correctAnswer)
    {
        if (Objects.equals(String.valueOf(Integer.parseInt(userHexString,16)), correctAnswer )) {
            return "Correct!";
        }
        else {
            return "Incorrect!";
        }
    }
}
