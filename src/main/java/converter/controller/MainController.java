package converter.controller;

import converter.model.User;
import converter.service.UserService;
import converter.validate.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserValidator userValidator;


    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public ModelAndView home(User user) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            return new ModelAndView("redirect:/user/tutorial-home", "user",user);
        }

        return new ModelAndView("/home");
    }

    @RequestMapping(value = "/join", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("user", new User());
        return "join";
    }

    @RequestMapping(value = "/join", method = RequestMethod.POST)
    public String postRegistration(@ModelAttribute("user") User user, BindingResult bindingResult) {
        userValidator.validate(user, bindingResult);

        if (bindingResult.hasErrors()) {
            return "join";
        }
        userService.save(user);
        return "login";
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error,
                              @RequestParam(value = "logout", required = false) String logout, Model model) {

        ModelAndView mav = new ModelAndView();
        mav.addObject("user", new User());
        if (error != null) {
            model.addAttribute("error", "Your username and password is invalid.");
        }
        if (logout != null) {
            model.addAttribute("message", "You have been logged out successfully.");
        }
        mav.setViewName("login");
        return mav;
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public String postLogin(@ModelAttribute("user") User user, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            return "login" + bindingResult.getAllErrors();
        }
        return "/user/account/user-account";
    }

    @RequestMapping(value = "/info")
    public String info() {
        return "info";
    }

    @RequestMapping(value = "/support", method = RequestMethod.GET)
    public String support() {
        return "support";
    }


}