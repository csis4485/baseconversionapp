package converter.controller;

import converter.model.HexadecimalToDecimalExam;
import converter.model.User;
import converter.service.GradeService;
import converter.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Objects;

@Controller
public class HexadecimalToDecimalController {

    @Autowired
    private UserService userService;
    private GradeService gradeService = new GradeService();
    private ArrayList<String> easyTestRandomList;
    private ArrayList<String> mediumTestRandomList;
    private ArrayList<String> hardTestRandomList;
    @RequestMapping(value = "/user/test/hexadecimal-decimal-test", method = RequestMethod.GET)
    public String decimalToHexadecimalTest(@ModelAttribute("user") User user, Model model,
                                           @RequestParam(value = "error", required = false) String error) {
        model.addAttribute("user", user);
        if (error != null){
            model.addAttribute("error", " Answer must be in decimal form!");
        }
        easyTestRandomList = new HexadecimalToDecimalExam().getEASY_RANDOM_LIST();
        mediumTestRandomList = new HexadecimalToDecimalExam().getMEDIUM_RANDOM_LIST();
        hardTestRandomList  = new HexadecimalToDecimalExam().getHARD_RANDOM_LIST();
        for (int i = 0; i < 5; i++) {
            model.addAttribute("easy_rand".concat(String.valueOf(i)),   easyTestRandomList.get(i).toUpperCase());
        }
        for (int i = 0; i < 5; i++) {
            model.addAttribute("medium_rand".concat(String.valueOf(i)), mediumTestRandomList.get(i).toUpperCase());
        }
        for (int i = 0; i < 5; i++) {
            model.addAttribute("hard_rand".concat(String.valueOf(i)),   hardTestRandomList.get(i).toUpperCase());
        }
        return "/user/test/hexadecimal-decimal-test";
    }

    @RequestMapping(value = "/user/test/hexadecimal-decimal-test", method = RequestMethod.POST)
    public String postDecimalToHexadecimalTest(@ModelAttribute("user") User user, HttpServletRequest request, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "/user/test/hexadecimal-decimal-test" + bindingResult.getAllErrors();
        }
        Principal principal = request.getUserPrincipal();
        User userDetails = userService.findByUsername(principal.getName());

        if(Objects.nonNull(user.getEasy1())){
            userDetails.setHexadecimalDecimalEasyGrade(gradeService.setGrade(user,"hexadecimalToDecimal",easyTestRandomList, true, false, false));
        }

        if(Objects.nonNull(user.getMedium1())){
            userDetails.setHexadecimalDecimalMediumGrade(gradeService.setGrade(user, "hexadecimalToDecimal",mediumTestRandomList, false, true, false));
        }

        if(Objects.nonNull(user.getHard1())){
            userDetails.setHexadecimalDecimalHardGrade(gradeService.setGrade(user, "hexadecimalToDecimal",hardTestRandomList, false, false, true));
        }
        userService.save(userDetails);
        return "redirect:/user/account/user-account";
    }

    @RequestMapping(value = "/user/tutorial/hexadecimal-decimal-tutorial", method = RequestMethod.GET)
    public String decimalToHexadecimalTutorial() {
        return "/user/tutorial/hexadecimal-decimal-tutorial";
    }
}
