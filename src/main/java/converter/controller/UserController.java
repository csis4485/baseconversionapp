package converter.controller;

import converter.model.User;
import converter.service.GradeService;
import converter.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Objects;

@Controller
public class UserController {

    @Autowired
    private UserService userService;
    private GradeService gradeService = new GradeService();


    @RequestMapping(value = "/user/account/user-account", method = RequestMethod.GET)
    public ModelAndView getUserAccount(@ModelAttribute("user") User user, HttpServletRequest request)  {

        ModelAndView mav = new ModelAndView();

        Principal principal = request.getUserPrincipal();
        mav.addObject("user", user);
        User userDetails = userService.findByUsername(principal.getName());
        mav.addObject("username", userDetails.getUsername());
        mav.addObject("email", userDetails.getEmail());
        if(userDetails.getJobTitle() != null) {
            mav.addObject("jobTitle", userDetails.getJobTitle());
        }
        if(userDetails.getJobDescription() != null) {
            mav.addObject("jobDescription", userDetails.getJobDescription());
        }
        if(userDetails.getJobStartDate() != null) {
            mav.addObject("jobStartDate", userDetails.getJobStartDate());
        }
        if(userDetails.getDegreeDate() != null) {
            mav.addObject("degreeDate", userDetails.getDegreeDate());
        }
        if(userDetails.getDegreeName() != null) {
            mav.addObject("degreeName", userDetails.getDegreeName());
        }

        addGradesToModel(mav, userDetails);
        mav.setViewName("/user/account/user-account");
        return mav;
    }
    @RequestMapping(value = "/user/account/user-account-update", method = RequestMethod.GET)
    public String getUpdateUserAccount(@ModelAttribute("user") User user) {
        return "/user/account/user-account-update";
    }

    @RequestMapping(value = "/user/account/user-account-update", method = RequestMethod.POST)
    public String updateUserAccount(@ModelAttribute("user") User user, HttpServletRequest request) {

        Principal principal = request.getUserPrincipal();
        User userDetails = userService.findByUsername(principal.getName());

        if(Objects.nonNull(user.getJobTitle())){
            userDetails.setJobTitle(user.getJobTitle());
        }
        if(Objects.nonNull(user.getJobStartDate())){
            userDetails.setJobStartDate(user.getJobStartDate());
        }
        if(Objects.nonNull(user.getJobDescription())){
            userDetails.setJobDescription(user.getJobDescription());
        }
        if(Objects.nonNull(user.getDegreeName())){
            userDetails.setDegreeName(user.getDegreeName());
        }
        if(Objects.nonNull(user.getDegreeDate())){
            userDetails.setDegreeDate(user.getDegreeDate());
        }
        userService.save(userDetails);
        return "redirect:/user/account/user-account";
    }

    @RequestMapping(value = {"/user/account/tutorial-home"}, method = RequestMethod.GET)
    public String tutorials_home(User user, Model model) {
        model.addAttribute("user", user);
        return "/user/account/tutorial-home";
    }

    @RequestMapping(value = "/user/account/in-progress", method = RequestMethod.GET)
    public String in_progress(@ModelAttribute("user") User user, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        Principal principal = request.getUserPrincipal();
        mav.addObject("user", user);
        User userDetails = userService.findByUsername(principal.getName());
        addGradesToModel(mav, userDetails);
        return "/user/account/in-progress";
    }
    @RequestMapping(value = "/user/support", method = RequestMethod.GET)
    public String support() {
        return "/user/support";
    }

    private void addGradesToModel(ModelAndView mav, User userDetails) {
        if(userDetails.getBinaryDecimalEasyGrade() != null) {
            mav.addObject("grade0", String.valueOf(userDetails.getBinaryDecimalEasyGrade()).concat("%"));
        }
        if(userDetails.getBinaryDecimalMediumGrade() != null) {
            mav.addObject("grade1", String.valueOf(userDetails.getBinaryDecimalMediumGrade()).concat("%"));
        }
        if(userDetails.getBinaryDecimalHardGrade() != null) {
            mav.addObject("grade2", String.valueOf(userDetails.getBinaryDecimalHardGrade()).concat("%"));
        }
        if(userDetails.getDecimalBinaryEasyGrade() != null) {
            mav.addObject("grade3", String.valueOf(userDetails.getDecimalBinaryEasyGrade()).concat("%"));
        }
        if(userDetails.getDecimalBinaryMediumGrade() != null) {
            mav.addObject("grade4", String.valueOf(userDetails.getDecimalBinaryMediumGrade()).concat("%"));
        }
        if(userDetails.getDecimalBinaryHardGrade() != null) {
            mav.addObject("grade5", String.valueOf(userDetails.getDecimalBinaryHardGrade()).concat("%"));
        }
        if(userDetails.getDecimalHexadecimalEasyGrade() != null) {
            mav.addObject("grade6", String.valueOf(userDetails.getDecimalHexadecimalEasyGrade()).concat("%"));
        }
        if(userDetails.getDecimalHexadecimalMediumGrade() != null) {
            mav.addObject("grade7", String.valueOf(userDetails.getDecimalHexadecimalMediumGrade()).concat("%"));
        }
        if(userDetails.getDecimalHexadecimalHardGrade() != null) {
            mav.addObject("grade8", String.valueOf(userDetails.getDecimalHexadecimalHardGrade()).concat("%"));
        }
        if(userDetails.getHexadecimalDecimalEasyGrade() != null) {
            mav.addObject("grade9", String.valueOf(userDetails.getHexadecimalDecimalEasyGrade()).concat("%"));
        }
        if(userDetails.getHexadecimalDecimalMediumGrade() != null) {
            mav.addObject("grade10", String.valueOf(userDetails.getHexadecimalDecimalMediumGrade()).concat("%"));
        }
        if(userDetails.getHexadecimalDecimalHardGrade() != null) {
            mav.addObject("grade11", String.valueOf(userDetails.getHexadecimalDecimalHardGrade()).concat("%"));
        }
    }
}