package converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String[] PERMITALL_RESOURCE_LIST = new String[] {"/error", "/support", "/nav", "/403" };
    private static final String[] ANON_RESOURCE_LIST = new String[] {"/login/**", "/login?logout","/login?expired", "/login?error","/join/**", "/reset_password", "/home","/", };
    private static final String[] IGNORED_RESOURCE_LIST = new String[] { "/img/**", "/static/**", "/css/**", "/js/**", "/tags/**", "/fragments/**"};
    private static final String[] USER_RESOURCE_LIST = new String[] { "/user/**" };

    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .anonymous().principal("ANON").authorities("ANON")
                .and()
                .authorizeRequests()
                .antMatchers(PERMITALL_RESOURCE_LIST).permitAll()
                .antMatchers(ANON_RESOURCE_LIST).hasAuthority("ANON")
                .antMatchers(USER_RESOURCE_LIST).authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/user/account/user-account")
                .failureUrl("/login?error")
                .and()
                .logout()
                .logoutSuccessUrl("/login?logout")
                .and()
                .rememberMe()
                .and()
                .sessionManagement()
                .maximumSessions(1)
                .expiredUrl("/login?expired");
    }

    /**
     * Want to ignore any URLs involved with our home page or fake login flow.
     * Also need to re-ignore static resources as they would normally be ignored by default,
     * but since we have this class we are overriding Spring Boot's defaults.
     * <p>
     * For default configuration that we are overriding,
     * see {@link org.springframework.boot.autoconfigure.security.SpringBootWebSecurityConfiguration}.
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(IGNORED_RESOURCE_LIST);
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }

}
