package converter.model;

import converter.conversion.HexadecimalToDecimal;
import converter.service.UtilityService;

import java.util.ArrayList;

public class HexadecimalToDecimalExam {

    private static HexadecimalToDecimal hexadecimalToDecimal = new HexadecimalToDecimal();
    private ArrayList<String> EASY_RANDOM_LIST;
    private ArrayList<String> MEDIUM_RANDOM_LIST;
    private ArrayList<String> HARD_RANDOM_LIST;

    public HexadecimalToDecimalExam() {
        this.EASY_RANDOM_LIST = new UtilityService().createStringArray(
                hexadecimalToDecimal.randomHexadecimalString(15),
                hexadecimalToDecimal.randomHexadecimalString(15),
                hexadecimalToDecimal.randomHexadecimalString(15),
                hexadecimalToDecimal.randomHexadecimalString(15),
                hexadecimalToDecimal.randomHexadecimalString(15)
        );
        this.MEDIUM_RANDOM_LIST = new UtilityService().createStringArray(
                hexadecimalToDecimal.randomHexadecimalString(31),
                hexadecimalToDecimal.randomHexadecimalString(31),
                hexadecimalToDecimal.randomHexadecimalString(31),
                hexadecimalToDecimal.randomHexadecimalString(31),
                hexadecimalToDecimal.randomHexadecimalString(31)
        );
        this.HARD_RANDOM_LIST = new UtilityService().createStringArray(
                hexadecimalToDecimal.randomHexadecimalString(1027),
                hexadecimalToDecimal.randomHexadecimalString(1027),
                hexadecimalToDecimal.randomHexadecimalString(1027),
                hexadecimalToDecimal.randomHexadecimalString(1027),
                hexadecimalToDecimal.randomHexadecimalString(1027)
        );
    }

    public ArrayList<String> getEASY_RANDOM_LIST() {
        return EASY_RANDOM_LIST;
    }

    public ArrayList<String> getMEDIUM_RANDOM_LIST() {
        return MEDIUM_RANDOM_LIST;
    }

    public ArrayList<String> getHARD_RANDOM_LIST() {
        return HARD_RANDOM_LIST;
    }
}
