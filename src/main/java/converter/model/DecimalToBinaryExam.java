package converter.model;


import converter.conversion.DecimalToBinary;
import converter.service.UtilityService;

import java.util.ArrayList;

public class DecimalToBinaryExam {

    private static DecimalToBinary decimalToBinary = new DecimalToBinary();
    private ArrayList<String> EASY_RANDOM_LIST;
    private ArrayList<String> MEDIUM_RANDOM_LIST;
    private ArrayList<String> HARD_RANDOM_LIST;

    public DecimalToBinaryExam() {
        this.EASY_RANDOM_LIST = new UtilityService().createStringArray(
                decimalToBinary.randomDecimalString(7),
                decimalToBinary.randomDecimalString(7),
                decimalToBinary.randomDecimalString(7),
                decimalToBinary.randomDecimalString(7),
                decimalToBinary.randomDecimalString(7)
        );
        this.MEDIUM_RANDOM_LIST = new UtilityService().createStringArray(
                decimalToBinary.randomDecimalString(31),
                decimalToBinary.randomDecimalString(31),
                decimalToBinary.randomDecimalString(31),
                decimalToBinary.randomDecimalString(31),
                decimalToBinary.randomDecimalString(31)
        );
        this.HARD_RANDOM_LIST = new UtilityService().createStringArray(
                decimalToBinary.randomDecimalString(1027),
                decimalToBinary.randomDecimalString(1027),
                decimalToBinary.randomDecimalString(1027),
                decimalToBinary.randomDecimalString(1027),
                decimalToBinary.randomDecimalString(1027)
        );

    }

    public ArrayList<String> getEASY_RANDOM_LIST() {
        return EASY_RANDOM_LIST;
    }

    public ArrayList<String> getMEDIUM_RANDOM_LIST() {
        return MEDIUM_RANDOM_LIST;
    }

    public ArrayList<String> getHARD_RANDOM_LIST() {
        return HARD_RANDOM_LIST;
    }
}
