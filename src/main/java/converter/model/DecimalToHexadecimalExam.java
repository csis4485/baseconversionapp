package converter.model;

import converter.conversion.DecimalToHexadecimal;
import converter.service.UtilityService;

import java.util.ArrayList;

public class DecimalToHexadecimalExam {

    private static DecimalToHexadecimal decimalToHexadecimal = new DecimalToHexadecimal();
    private ArrayList<String> EASY_RANDOM_LIST;
    private ArrayList<String> MEDIUM_RANDOM_LIST;
    private ArrayList<String> HARD_RANDOM_LIST;

    public DecimalToHexadecimalExam() {
        this.EASY_RANDOM_LIST = new UtilityService().createStringArray(
                decimalToHexadecimal.randomDecimalString(15),
                decimalToHexadecimal.randomDecimalString(15),
                decimalToHexadecimal.randomDecimalString(15),
                decimalToHexadecimal.randomDecimalString(15),
                decimalToHexadecimal.randomDecimalString(15)
        );
        this.MEDIUM_RANDOM_LIST = new UtilityService().createStringArray(
                decimalToHexadecimal.randomDecimalString(31),
                decimalToHexadecimal.randomDecimalString(31),
                decimalToHexadecimal.randomDecimalString(31),
                decimalToHexadecimal.randomDecimalString(31),
                decimalToHexadecimal.randomDecimalString(31)
        );
        this.HARD_RANDOM_LIST = new UtilityService().createStringArray(
                decimalToHexadecimal.randomDecimalString(1027),
                decimalToHexadecimal.randomDecimalString(1027),
                decimalToHexadecimal.randomDecimalString(1027),
                decimalToHexadecimal.randomDecimalString(1027),
                decimalToHexadecimal.randomDecimalString(1027)
        );

    }

    public ArrayList<String> getEASY_RANDOM_LIST() {
        return EASY_RANDOM_LIST;
    }

    public ArrayList<String> getMEDIUM_RANDOM_LIST() {
        return MEDIUM_RANDOM_LIST;
    }

    public ArrayList<String> getHARD_RANDOM_LIST() {
        return HARD_RANDOM_LIST;
    }
}