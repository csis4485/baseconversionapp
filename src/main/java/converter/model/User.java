package converter.model;

import converter.service.UtilityService;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;


@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String username;
    private String email;
    private String password;
    private String jobTitle;
    private String jobDescription;
    private String jobStartDate;
    private String degreeName;
    private String degreeDate;
    private Integer binaryDecimalEasyGrade;
    private Integer binaryDecimalMediumGrade;
    private Integer binaryDecimalHardGrade;
    private Integer decimalBinaryEasyGrade;
    private Integer decimalBinaryMediumGrade;
    private Integer decimalBinaryHardGrade;
    private Integer decimalHexadecimalEasyGrade;
    private Integer decimalHexadecimalMediumGrade;
    private Integer decimalHexadecimalHardGrade;
    private Integer hexadecimalDecimalEasyGrade;
    private Integer hexadecimalDecimalMediumGrade;
    private Integer hexadecimalDecimalHardGrade;
    private boolean enabled;
    private boolean tokenExpired;
    private String easy1;
    private String easy2;
    private String easy3;
    private String easy4;
    private String easy5;
    private String medium1;
    private String medium2;
    private String medium3;
    private String medium4;
    private String medium5;
    private String hard1;
    private String hard2;
    private String hard3;
    private String hard4;
    private String hard5;


    @ManyToMany
    @JoinTable(name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private Collection<Role> roles;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getBinaryDecimalEasyGrade() {
        return binaryDecimalEasyGrade;
    }

    public void setBinaryDecimalEasyGrade(Integer binaryDecimalEasyGrade) {
        this.binaryDecimalEasyGrade = binaryDecimalEasyGrade;
    }

    public Integer getBinaryDecimalMediumGrade() {
        return binaryDecimalMediumGrade;
    }

    public void setBinaryDecimalMediumGrade(Integer binaryDecimalMediumGrade) {
        this.binaryDecimalMediumGrade = binaryDecimalMediumGrade;
    }

    public Integer getBinaryDecimalHardGrade() {
        return binaryDecimalHardGrade;
    }

    public void setBinaryDecimalHardGrade(Integer binaryDecimalHardGrade) {
        this.binaryDecimalHardGrade = binaryDecimalHardGrade;
    }

    public ArrayList<String> getEasyAnswers() {
        return new UtilityService().createStringArray(easy1,easy2, easy3, easy4,easy5);
    }

    public void setEasyAnswers(ArrayList<String> easyAnswers) {
            this.easy1 = easyAnswers.get(0);
            this.easy2 = easyAnswers.get(1);
            this.easy3 = easyAnswers.get(2);
            this.easy4 = easyAnswers.get(3);
            this.easy5 = easyAnswers.get(4);
    }

    public ArrayList<String> getMediumAnswers() {
        return new UtilityService().createStringArray(medium1,medium2, medium3, medium4,medium5);
    }

    public void setMediumAnswers(ArrayList<String> mediumAnswers) {
        this.medium1 = mediumAnswers.get(0);
        this.medium2 = mediumAnswers.get(1);
        this.medium3 = mediumAnswers.get(2);
        this.medium4 = mediumAnswers.get(3);
        this.medium5 = mediumAnswers.get(4);
    }

    public ArrayList<String> getHardAnswers() {
        return new UtilityService().createStringArray(hard1,hard2, hard3, hard4,hard5);
    }

    public void setHardAnswers(ArrayList<String> hardAnswers) {
        this.hard1 = hardAnswers.get(0);
        this.hard2 = hardAnswers.get(1);
        this.hard3 = hardAnswers.get(2);
        this.hard4 = hardAnswers.get(3);
        this.hard5 = hardAnswers.get(4);    }

    public String getEasy1() {
        return easy1;
    }

    public String getMedium1() {
        return medium1;
    }

    public String getHard1() {
        return hard1;
    }

    public Integer getDecimalBinaryEasyGrade() {
        return decimalBinaryEasyGrade;
    }

    public void setDecimalBinaryEasyGrade(Integer decimalBinaryEasyGrade) {
        this.decimalBinaryEasyGrade = decimalBinaryEasyGrade;
    }

    public Integer getDecimalBinaryMediumGrade() {
        return decimalBinaryMediumGrade;
    }

    public void setDecimalBinaryMediumGrade(Integer decimalBinaryMediumGrade) {
        this.decimalBinaryMediumGrade = decimalBinaryMediumGrade;
    }

    public Integer getDecimalBinaryHardGrade() {
        return decimalBinaryHardGrade;
    }

    public void setDecimalBinaryHardGrade(Integer decimalBinaryHardGrade) {
        this.decimalBinaryHardGrade = decimalBinaryHardGrade;
    }

    public Integer getDecimalHexadecimalEasyGrade() {
        return decimalHexadecimalEasyGrade;
    }

    public void setDecimalHexadecimalEasyGrade(Integer decimalHexadecimalEasyGrade) {
        this.decimalHexadecimalEasyGrade = decimalHexadecimalEasyGrade;
    }

    public Integer getDecimalHexadecimalMediumGrade() {
        return decimalHexadecimalMediumGrade;
    }

    public void setDecimalHexadecimalMediumGrade(Integer decimalHexadecimalMediumGrade) {
        this.decimalHexadecimalMediumGrade = decimalHexadecimalMediumGrade;
    }

    public Integer getDecimalHexadecimalHardGrade() {
        return decimalHexadecimalHardGrade;
    }

    public void setDecimalHexadecimalHardGrade(Integer decimalHexadecimalHardGrade) {
        this.decimalHexadecimalHardGrade = decimalHexadecimalHardGrade;
    }

    public Integer getHexadecimalDecimalEasyGrade() {
        return hexadecimalDecimalEasyGrade;
    }

    public void setHexadecimalDecimalEasyGrade(Integer hexadecimalDecimalEasyGrade) {
        this.hexadecimalDecimalEasyGrade = hexadecimalDecimalEasyGrade;
    }

    public Integer getHexadecimalDecimalMediumGrade() {
        return hexadecimalDecimalMediumGrade;
    }

    public void setHexadecimalDecimalMediumGrade(Integer hexadecimalDecimalMediumGrade) {
        this.hexadecimalDecimalMediumGrade = hexadecimalDecimalMediumGrade;
    }

    public Integer getHexadecimalDecimalHardGrade() {
        return hexadecimalDecimalHardGrade;
    }

    public void setHexadecimalDecimalHardGrade(Integer hexadecimalDecimalHardGrade) {
        this.hexadecimalDecimalHardGrade = hexadecimalDecimalHardGrade;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isTokenExpired() {
        return tokenExpired;
    }

    public void setTokenExpired(boolean tokenExpired) {
        this.tokenExpired = tokenExpired;
    }

    public void setEasy1(String easy1) {
        this.easy1 = easy1;
    }

    public String getEasy2() {
        return easy2;
    }

    public void setEasy2(String easy2) {
        this.easy2 = easy2;
    }

    public String getEasy3() {
        return easy3;
    }

    public void setEasy3(String easy3) {
        this.easy3 = easy3;
    }

    public String getEasy4() {
        return easy4;
    }

    public void setEasy4(String easy4) {
        this.easy4 = easy4;
    }

    public String getEasy5() {
        return easy5;
    }

    public void setEasy5(String easy5) {
        this.easy5 = easy5;
    }

    public void setMedium1(String medium1) {
        this.medium1 = medium1;
    }

    public String getMedium2() {
        return medium2;
    }

    public void setMedium2(String medium2) {
        this.medium2 = medium2;
    }

    public String getMedium3() {
        return medium3;
    }

    public void setMedium3(String medium3) {
        this.medium3 = medium3;
    }

    public String getMedium4() {
        return medium4;
    }

    public void setMedium4(String medium4) {
        this.medium4 = medium4;
    }

    public String getMedium5() {
        return medium5;
    }

    public void setMedium5(String medium5) {
        this.medium5 = medium5;
    }

    public void setHard1(String hard1) {
        this.hard1 = hard1;
    }

    public String getHard2() {
        return hard2;
    }

    public void setHard2(String hard2) {
        this.hard2 = hard2;
    }

    public String getHard3() {
        return hard3;
    }

    public void setHard3(String hard3) {
        this.hard3 = hard3;
    }

    public String getHard4() {
        return hard4;
    }

    public void setHard4(String hard4) {
        this.hard4 = hard4;
    }

    public String getHard5() {
        return hard5;
    }

    public void setHard5(String hard5) {
        this.hard5 = hard5;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getJobStartDate() {
        return jobStartDate;
    }

    public void setJobStartDate(String jobStartDate) {
        this.jobStartDate = jobStartDate;
    }

    public String getDegreeName() {
        return degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    public String getDegreeDate() {
        return degreeDate;
    }

    public void setDegreeDate(String degreeDate) {
        this.degreeDate = degreeDate;
    }
}