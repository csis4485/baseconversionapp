package converter.model;

import converter.conversion.BinaryToDecimal;
import converter.service.UtilityService;

import java.util.ArrayList;

public class BinaryToDecimalExam {
    private static BinaryToDecimal binaryToDecimal = new BinaryToDecimal();

    private ArrayList<String> EASY_RANDOM_LIST;
    private ArrayList<String> MEDIUM_RANDOM_LIST;
    private ArrayList<String> HARD_RANDOM_LIST;

    public BinaryToDecimalExam() {
        this.EASY_RANDOM_LIST = new UtilityService().createStringArray(
                binaryToDecimal.randomBinaryString(7),
                binaryToDecimal.randomBinaryString(7),
                binaryToDecimal.randomBinaryString(7),
                binaryToDecimal.randomBinaryString(7),
                binaryToDecimal.randomBinaryString(7)
        );
        this.MEDIUM_RANDOM_LIST = new UtilityService().createStringArray(
                binaryToDecimal.randomBinaryString(31),
                binaryToDecimal.randomBinaryString(31),
                binaryToDecimal.randomBinaryString(31),
                binaryToDecimal.randomBinaryString(31),
                binaryToDecimal.randomBinaryString(31)
        );
        this.HARD_RANDOM_LIST = new UtilityService().createStringArray(
                binaryToDecimal.randomBinaryString(1027),
                binaryToDecimal.randomBinaryString(1027),
                binaryToDecimal.randomBinaryString(1027),
                binaryToDecimal.randomBinaryString(1027),
                binaryToDecimal.randomBinaryString(1027)
        );

    }

    public ArrayList<String> getEASY_RANDOM_LIST() {
        return EASY_RANDOM_LIST;
    }

    public ArrayList<String> getMEDIUM_RANDOM_LIST() {return MEDIUM_RANDOM_LIST;}

    public ArrayList<String> getHARD_RANDOM_LIST() {
        return HARD_RANDOM_LIST;
    }
}
